fetch("http://hackergramapi.herokuapp.com/graph")
  .then((res) => res.json())
  .then((data) => {
    ReactDOM.render(
      <ForceGraph3D
        graphData={data}
        nodeLabel="id"
        nodeAutoColorBy="group"
        /*linkThreeObjectExtend={true}

          linkThreeObject={link => {
            // extend link with text sprite
            const sprite = new SpriteText(`${link.weight}`);
            sprite.color = 'lightgrey';
            sprite.textHeight = 1.5;
            return sprite;
          }}*/
        nodeThreeObject={(node) => {
          const sprite = new SpriteText(node.id);
          sprite.color = node.color;
          sprite.textHeight = 2;
          return sprite;
        }}
        linkDirectionalArrowLength={1.5}
        linkDirectionalArrowRelPos={1}
        linkWidth={1}
        linkCurvature="curvature"
        linkLabel={(link) => {
          return link.weight;
        }}
        linkAutoColorBy="weight"

        /*linkPositionUpdate={(sprite, { start, end }) => {
            const middlePos = Object.assign(...['x', 'y', 'z'].map(c => ({
              [c]: start[c] + (end[c] - start[c]) / 2 // calc middle point
            })));


            // Position sprite
            Object.assign(sprite.position, middlePos);
          }}*/
      />,
      document.getElementById("graph")
    );
  });
